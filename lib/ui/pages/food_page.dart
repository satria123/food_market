part of 'pages.dart';

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              color: Colors.white,
              height: 100,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Food Market', style: blackFontStyle1),
                      Text('Lets get some foods',
                          style: greyFontStyle.copyWith(
                              fontWeight: FontWeight.w300))
                    ],
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: NetworkImage(
                                'https://cdns.klimg.com/newshub.id/news/2019/01/03/164552/laras-permatasari-influencer-cantik-yang-enggak-neko-neko-dalam-berpenampilan-1901033.jpg'),
                                fit: BoxFit.cover)),
                  )
                ],
              ),
            ),
            FoodCard()
          ],
        )
      ],
    );
  }
}
